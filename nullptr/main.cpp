#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

using namespace std;

void use(int* ptr)
{
    if (ptr != nullptr)
        cout << "use(int*: " << *ptr << ")" << endl;
}

void use(long value)
{
    cout << "use(int: " << value << ")" << endl;
}

void use(nullptr_t) = delete;

void calc(int x)
{
    cout << "calc(int: " << x << ")" << endl;
}

void calc(float) = delete;

TEST_CASE("NULL in C++98")
{
    int x = 10;
    use(&x);
    use(x);
    use(NULL);
    // use(nullptr); // ERROR
}

TEST_CASE("calc(double) can be deleted")
{
    //calc(3.14); // ERROR
}

TEST_CASE("init of pointers")
{
    SECTION("C++98")
    {
        int* ptr1 = NULL;
    }

    SECTION("since c++11")
    {
        int* ptr2 = nullptr;
        REQUIRE(ptr2 == nullptr);

        int* ptr3{};
        REQUIRE(ptr3 == nullptr);
    }
}
