#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

using namespace std;

TEST_CASE("initializer list")
{
    SECTION("C++98")
    {
        vector<int> vec;
        vec.push_back(1);
        vec.push_back(2);
        vec.push_back(3);
        vec.push_back(4);
    }

    SECTION("Since C++11")
    {
        vector<int> vec = { 1, 2, 3, 4, 5 }; // initializer_list<int>
    }

    SECTION("is interpreted as")
    {
        initializer_list<int> il = { 1, 2, 3, 4 };
        vector<int> vec(il);

        const int* start_pos = il.begin();
        const int* end_pos = il.end();
        REQUIRE(il.size() == 4);
    }

    SECTION("creating large vector with il")
    {
        vector<int> vec;
        vec.reserve(1000);

        vec.insert(vec.begin(), { 1, 2, 3, 4, 5, 6 });
    }

    SECTION("can be used in range-based-for")
    {
        for(int item : { 1, 2, 3, 5, 7 })
        {
            cout << item << " ";
        }
        cout << endl;
    }
}

class Container
{
    size_t size_;
    int* items_;
public:
    //typedef int* iterator;
    using iterator = int*;
    using const_iterator = const int*;

    Container(initializer_list<int> il)
        : size_{il.size()}, items_{new int[il.size()]}
    {
        std::copy(il.begin(), il.end(), begin());
    }

    ~Container()
    {
        delete [] items_;
    }

    iterator begin()
    {
        //decltype(this) -> Container*
        return items_;
    }

    const_iterator begin() const
    {
        return items_;
    }

    const_iterator cbegin() const
    {
        return items_;
    }

    iterator end()
    {
        return items_ + size_;
    }

    const_iterator end() const
    {
        return items_ + size_;
    }

    const_iterator cend() const
    {
        return items_ + size_;
    }
};

TEST_CASE("custom container")
{
    Container c = { 1, 2, 3, 4, 5 };

    for(const auto& item : c)
        cout << item << " ";
    cout << endl;

    for(auto& item : c)
        item = 665;

    for(auto it = c.cbegin(); it != c.cend(); ++it)
        cout << *it << " ";
    cout << endl;
}

TEST_CASE("il with auto")
{
    auto numbers = { 1, 2, 3, 4, 5 }; // special case for auto
    static_assert(is_same<initializer_list<int>, decltype(numbers)>::value, "ERROR");

    SECTION("C++11")
    {
        auto a = {1}; // initializer_list<int>
        auto b(10); // int
        auto c{10}; // initializer_list<int>
    }

    SECTION("C++17")
    {
        auto a = {1}; // initializer_list<int>
        auto b(10); // int
        auto c{10}; // int
    }
}
