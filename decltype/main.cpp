#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>
#include <map>

using namespace std;

TEST_CASE("decltype")
{
    map<int, string> dict = { { 1, "one"}, {2, "two"}, {3, "three"} };

    auto dict2 = dict;

    REQUIRE(dict2[1] == "one");

    decltype(dict) dict3;

    REQUIRE(dict3.empty());

    dict3[0];

    REQUIRE(dict3.size() == 1);

    decltype(dict3[0]) ref_str = dict[1];
}

struct Vector2D
{
    double x, y;
};

double operator*(const Vector2D& a, const Vector2D& b)
{
    return a.x * b.x + a.y * b.y;
}

template <typename T>
auto multiply(const T& a, const T& b) -> decltype(a * b)
{
    return a * b;
}

// since C++14
template <typename T>
auto add(const T& a, const T& b)
{
    return a + b;
}

auto describe(int x)// -> string
{
    if (x % 2 == 0)
        return "even"s;
    else
        return string("odd");
}

TEST_CASE("vector multiplication")
{
    Vector2D a{1, 2};
    Vector2D b{1, 2};

    double scalar = a * b;
}
