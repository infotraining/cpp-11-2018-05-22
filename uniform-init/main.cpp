#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

using namespace std;

int x(); // decalration not initialization
int y{}; // zero value

struct CPoint
{
    int x, y;
};

struct CppPoint
{
    int x, y;

    CppPoint(int x, int y) : x(x), y(y)
    {}
};

TEST_CASE("initialization in C++98")
{
   int x1;
   int x2(5);
   int x3 = 4;

   CPoint pt1{1, 2};
   CppPoint pt2(6, 5);

   int tab[10] = { 1, 2, 4, 5 };

   vector<int> vec;
   vec.push_back(1);
   vec.push_back(2);
}

TEST_CASE("uniform initalization syntax")
{
    int x1;
    int x2{5};

    CPoint pt1{1, 2};
    CppPoint pt2{6, 5}; // constructor called

    int tab[10] = { 1, 2, 3, 4, 5 };
    vector<int> vec = { 1, 2, 3, 4, 5 };

    SECTION("issue with il")
    {
        vector<int> vec1(3, 5);
        REQUIRE_THAT(vec1, Catch::Matchers::Equals(vector<int>{5, 5, 5}));

        vector<int> vec2{3, 5};
        REQUIRE_THAT(vec2, Catch::Matchers::Equals(vector<int>{3, 5}));
    }
}

TEST_CASE("narrowing conversion is an error")
{
    int x = 255;
    char c{x};
}
