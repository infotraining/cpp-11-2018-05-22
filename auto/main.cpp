#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>
#include <type_traits>
#include <list>

using namespace std;

TEST_CASE("auto")
{
   auto x = 10; // int
   auto y = 3.14; // double
   auto z = x * y; // double

   static_assert(is_same<double, decltype(z)>::value, "Error:  types are different");

   static_assert(sizeof(int) == 4, "int must be size of four bytes");

   SECTION("can use const, volatile modifiers")
   {
       const auto dx = 0.01; // const double
       volatile auto temp = 523.0; // volatile double
   }

   SECTION("using auto with containers")
   {
        vector<int> vec = { 1, 2, 3 };

        SECTION("C++98")
        {
            for(vector<int>::const_iterator it = vec.begin(); it != vec.end(); ++it)
               cout << *it << endl;
        }

        SECTION("since C++11")
        {
            for(auto it = vec.cbegin(); it != vec.cend(); ++it)
                cout << *it << endl;
        }
   }
}


TEST_CASE("C++20")
{
//    template<typename T>
//    concept Addable =
//    requires (T a, T b) {
//        a + b; // "the expression a+b is a valid expression that will compile"
//    };

//    template <Addable>
//    void foo(Addable item)
//    {
//        cout << "foo(" << (item + item) << ")" << endl;
//    }
}

template <typename T>
void foo(T item)
{
   cout << "foo(" << (item + item) << ")" << endl;
}

TEST_CASE("argument deduction in template functions")
{
    foo(10);
    foo(3.14);
    foo(string("text"));

    int x = 10;
    foo(x * 3.14F);

//    vector<int> vec;
//    foo(vec); // ERROR
}


TEST_CASE("auto rules")
{
    int x = 10;
    const double dx = 0.1;
    int& ref_x = x;
    char text[255] = {};

    SECTION("Case 1")
    {
        auto ax = x; // int
        auto adx = dx; // double
        auto new_x = ref_x; // int
        auto atext = text; // char* - decay to pointer - for tables
        auto fun_ptr = foo<int>; // void (*)(int) // decay to pointer - for functions
    }

    SECTION("Case 2")
    {
        auto& aref_x = x; //int&
        auto& aref_dx = dx; // const double&
        auto* ptr_dx = &dx; // const double*
        auto& newref_x = ref_x; // int&
        auto& ref_text = text; // char (&)[255]
        auto& ref_fun = foo<int>; // void (&)(int)
    }
}

TEST_CASE("auto supports two syntax")
{
    int x = 10;

    SECTION("copy syntax")
    {
        auto ax = x;
    }

    SECTION("direct init syntax")
    {
        auto ax(x);
    }
}

TEST_CASE("range-based for")
{
    vector<int> vec = { 1, 2, 3, 4 };

    SECTION("C++98")
    {
        for(vector<int>::const_iterator it = vec.begin(); it != vec.end(); ++it)
        {
            cout << *it << " ";
        }
        cout << endl;
    }

    SECTION("C++11")
    {
        for(int item : vec)
            cout << item << " ";
        cout << endl;

        SECTION("compiler interprets as")
        {
            for(auto it = vec.begin(); it != vec.end(); ++it)
            {
                int item = *it;
                cout << *it << " ";
            }
        }
    }

    SECTION("works with static arrays")
    {
        int numbers[10] = { 1, 2, 3, 4 };

        for(int n : numbers)
            cout << n << " ";
        cout << endl;

        SECTION("compiler interprets as")
        {
            for(auto it = begin(numbers); it != end(numbers); ++it)
            {
                int n = *it;
                cout << *it << " ";
            }
        }
    }
}

TEST_CASE("efficient use")
{
    vector<string> words = { "one", "two", "three" };

    SECTION("BEWARE - INEFFICIENT")
    {
        for(string w : words)
        {
            cout << w << " ";
        }
        cout << endl;
    }

    SECTION("EFFICIENT - read-only")
    {
        for(const string& w : words)
        {
            cout << w << " ";
        }
        cout << endl;
    }

    SECTION("EFFICIENT - read-write")
    {
        for(string& w : words)
        {
            w += "!";
            cout << w << " ";
        }
        cout << endl;

        REQUIRE(words[0] == "one!");
    }
}

TEST_CASE("range-based-for with auto")
{
    vector<string> words = { "one", "two", "three" };

    for(const auto& w : words)
    {
        cout << w << " ";
    }
    cout << endl;
}

struct Container
{
    int tab[10] = { 1, 2, 3 };

//    int* begin()
//    {
//        return tab;
//    }

//    int* end()
//    {
//        return tab + 10;
//    }
};

int* begin(Container& c)
{
    return c.tab;
}

int* end(Container& c)
{
    return c.tab + 10;
}

TEST_CASE("range-based-for for custom types")
{
    Container cont;

    for(const auto& item : cont)
    {
        cout << item << " ";
    }
    cout << endl;
}
