#ifndef CONTAINER_HPP
#define CONTAINER_HPP

#include <initializer_list>

class Container
{
    std::size_t size_;
    int* items_;
public:
    //typedef int* iterator;
    using iterator = int*;
    using const_iterator = const int*;

    Container(std::initializer_list<int> il)
        : size_{il.size()}, items_{new int[il.size()]}
    {
        std::copy(il.begin(), il.end(), begin());
    }

    Container(const Container&) = delete;
    Container& operator=(const Container&) = delete;

    ~Container()
    {
        delete [] items_;
    }

    iterator begin()
    {
        //decltype(this) -> Container*
        return items_;
    }

    const_iterator begin() const
    {
        return items_;
    }

    const_iterator cbegin() const
    {
        return items_;
    }

    iterator end()
    {
        return items_ + size_;
    }

    const_iterator end() const
    {
        return items_ + size_;
    }

    const_iterator cend() const
    {
        return items_ + size_;
    }
};


#endif //CONTAINER_HPP
