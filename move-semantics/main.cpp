#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

using namespace std;

string full_name(const string& fn, const string& ln)
{
    return fn + " " + ln;
}

TEST_CASE("reference binding")
{
    string first_name = "Jan";
    string last_name = "Kowalski";

    SECTION("C++98")
    {
        string& ref_name = first_name;
        const string& ref_fullname = full_name(first_name, "Nowak");
    }

    SECTION("C++11")
    {
        string&& rvref_fullname = full_name(first_name, last_name);
        rvref_fullname += "!";
        cout << rvref_fullname << endl;

        // string&& rvref_name = first_name; - ERROR
    }
}

namespace Legacy
{
    void use(string* str)
    {
        if (str)
            cout << *str << endl;
    }

    string* load_file()
    {
        return new string("file");
    }
}

template <typename T>
class SmartPtr
{
    T* ptr_;
public:
    explicit SmartPtr(T* ptr) : ptr_{ptr}
    {
        cout << "Acquiring ownership of object: " << ptr_ << endl;
    }

    SmartPtr(const SmartPtr&) = delete;
    SmartPtr& operator=(const SmartPtr&) = delete;

    SmartPtr(SmartPtr&& other)
        : ptr_{other.ptr_}
    {
        other.ptr_ = nullptr;
        cout << "Moving an object: " << ptr_ << " to another pointer\n";
    }

    SmartPtr& operator=(SmartPtr&& other)
    {
        if (this != &other)
        {
            cout << "Deleting an object: " << ptr_ << endl;
            delete ptr_; // delete previous state

            ptr_ = other.ptr_; // transfer state from other
            other.ptr_ = nullptr;

            cout << "Moving an object by =: " << ptr_ << " to another pointer\n";
        }

        return *this;
    }

    T& operator*() const
    {
        return *ptr_;
    }

    T* operator->() const
    {
        return ptr_;
    }

    T* get() const
    {
        return ptr_;
    }

    ~SmartPtr()
    {
        cout << "Deleting an object: " << ptr_ << endl;

        delete ptr_;
    }
};

namespace Modern
{
    void use(string* str)
    {
        if (str)
            cout << *str << endl;
    }

    void use(string& str)
    {
        cout << str << endl;
    }

    void use(SmartPtr<string> str)
    {
        if (str.get())
        {
            cout << *str << endl;
        }
    }

    SmartPtr<string> load_file()
    {
        return SmartPtr<string>{new string("file")};
    }
}

TEST_CASE("smart pointer")
{
    SECTION("legacy code")
    {
        using namespace Legacy;

        string* str = new string("text");

        use(str);
        use(new string("leak")); // leak
        use(load_file()); // leak

        string s = "stack";
        use(&s); // maybe coredump

        delete str;
    }

    SECTION("modern code")
    {
        using namespace Modern;

        SmartPtr<string> str{new string("text")};

        cout << *str << endl;
        cout << str->size() << endl;

        SmartPtr<string> other = std::move(str); // mc ctor
        cout << "other: " << *other << endl;

        str = std::move(other); // mv op=

        use(str.get());
        use(*str);
        use(std::move(str));
        use(load_file());
    }
}
