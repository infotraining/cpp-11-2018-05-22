#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>
#include <chrono>

using namespace std;

TEST_CASE("common literal")
{
    string str = "C:\\nasz\\twoj\\backup_file.cpp";

    cout << str << endl;
}

TEST_CASE("raw string literal")
{
    string str = R"(C:\nasz\twoj\backup_file.cpp)";

    cout << str << endl;

    string multiline_str =
R"(Line1
Line2
Line3)" "\nLine4";

    cout << multiline_str << endl;
}

TEST_CASE("custom delimiters")
{
    string quote = R"radmor(quote: "(this is sentence)")radmor";

    cout << quote << endl;
}

TEST_CASE("string literal in C++14")
{
    auto text = "abc"; // const char*

    auto text2 = "abc"s;
}

struct Length
{
    long double value;
};

Length operator ""_m(long double value)
{
    return Length{value};
}

struct Velocity
{
    double value;
};

Velocity operator/(Length s, chrono::seconds t)
{
    return Velocity{ s.value / t.count() };
}

TEST_CASE("")
{
    auto length = 100.0_m;
    auto time = 100s;

    auto velocity = 100.0_m / 10s;
}
