#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

using namespace std;

enum DaysOfWeek : uint8_t; // forward declaration - since C++11

void use(DaysOfWeek day); // function declaration

enum DaysOfWeek : uint8_t { mon = 1, tue, wed, thd, fri, sat, sun };

void use(DaysOfWeek day)
{
    if (day == mon)
        cout << "Monday" << endl;
}

int foo(DaysOfWeek);

TEST_CASE("classic enums")
{
    DaysOfWeek day = mon;

    uint8_t index = 5;

    index = sat;
}

enum class Engine : uint8_t { petrol, diesel, hybrid };

TEST_CASE("scoped enums")
{
    Engine e = Engine::diesel;

    e = static_cast<Engine>(2);
    auto index = static_cast<int>(e);
}
