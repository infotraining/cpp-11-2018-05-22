#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

#include "container.hpp"

using namespace std;

class IGadget
{
public:
    virtual void use() const = 0;
    virtual ~IGadget() = default;
};

class Gadget : public IGadget
{
    int id_{};
    string name_ = "unknown";

public:
    Gadget() = default;
    virtual ~Gadget() = default;

    Gadget(int id, const string& name)
        : id_{id}, name_{name}
    {}

    Gadget(const string& name)
        : Gadget{665, name} // delegating contruction to another constructor - since C++11
    {
        name_ += "!";
    }

    void use() const override
    {
        cout << "Using gadget(" << id_ << ", " << name_ << ")\n";
    }

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }
};

class SuperGadget : public Gadget
{
public:
    using Gadget::Gadget; // constructors inheritance - since C++11

    void use() const final override
    {
        cout << "Using super gadget(" << id() << ", " << name() << ")" << endl;
    }
};

TEST_CASE("default constructor")
{
    Gadget g1{1, "ipad"};
    g1.use();

    Gadget g2;
    g2.use();

    Gadget g3{"smartwatch"};
    g3.use();

    SuperGadget g4{2, "super ipad"};

    IGadget& ref_g = g4;
    ref_g.use();
}

class NoCopyable
{
public:
    NoCopyable() = default;
    NoCopyable(const NoCopyable&) = delete;
    NoCopyable& operator=(const NoCopyable&) = delete;
};

TEST_CASE("no-copyable objects")
{
    NoCopyable nc;

    //NoCopyable nc2 = nc;
}

TEST_CASE("container")
{
    Container c = { 1, 2, 3 };
}
